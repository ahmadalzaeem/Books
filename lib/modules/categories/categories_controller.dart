import 'package:flutter_course/config/user_information.dart';
import 'package:flutter_course/models/categories_model.dart';
import 'package:flutter_course/modules/categories/categories_service.dart';
import 'package:get/get.dart';

class CategoriesController extends GetxController{
  List<Category> categoriesList = [];
  CategoriesService _service = CategoriesService();
  var isLoading  = true.obs;
  @override
  void onInit() {
    super.onInit();
  }
  @override
  void onReady() async {
    categoriesList = await _service.getCategories(UserInformation.USER_TOKEN) ;
    isLoading(false);
    super.onReady();
  }

}