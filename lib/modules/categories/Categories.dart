import 'package:flutter_course/components/categories_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_course/constant.dart';
import 'package:get/get.dart';
import 'package:flutter_course/modules/categories/categories_controller.dart';

class Categories extends StatelessWidget {
  CategoriesController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            decoration: gradiantBackground,
            child: Obx(() {
              if (controller.isLoading.isTrue) {
                return Center(
                  child: CircularProgressIndicator(
                    color: Colors.white,
                    strokeWidth: 5,
                  ),
                );
              }
              return ListView(
                children: [
                  SizedBox(
                    height: 100,
                  ),
                  GridView.count(
                    physics: ClampingScrollPhysics(),
                    padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    shrinkWrap: true,
                    crossAxisCount: 2,
                    mainAxisSpacing: 20,
                    crossAxisSpacing: 20,
                    children: List.generate(controller.categoriesList.length,
                        (index) {
                      return CategoryCard(
                        text: '${controller.categoriesList[index].title}',
                        width: MediaQuery.of(context).size.width * 0.3,
                        height: MediaQuery.of(context).size.width * 0.3,
                        onTap: () {
                          print(controller.categoriesList[index].title);
                        },
                      );
                    }),
                  ),
                ],
              );
            })),
      ),
    );
  }
}
