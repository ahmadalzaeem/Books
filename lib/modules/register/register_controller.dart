import 'package:flutter_course/models/user.dart';
import 'package:flutter_course/modules/register/register_service.dart';
import 'package:get/get.dart';

class RegisterController extends GetxController {
  var fullName = '';
  var email = '';
  var password = '';
  var passwordConfirm = '';
  var registerStatus;
  var message;

  RegisterService service = RegisterService();

  Future<void> registerOnClick() async {
    User user = User(
      name: fullName,
      email: email,
      password: password,
      passwordConfirmation: passwordConfirm,
    );
    registerStatus = await service.register(user);
    message = service.message;
    if (message is List) {
      String temp = '';
      for (String s in message) temp += s + '\n';
      message = temp;
    }
  }
}
