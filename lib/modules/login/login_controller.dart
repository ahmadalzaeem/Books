import 'package:flutter_course/models/user.dart';
import 'package:flutter_course/modules/login/loginService.dart';
import 'package:flutter_course/native_service/secure_storage.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  late String email ;
  var password ;
  var loginStatus ;
  var message;
  var checkBoxStatus;

  late LoginService service ;


  @override
  void onInit() {
    email = '';
    password = '';
    loginStatus = false;
    message = '';
    checkBoxStatus = false.obs;
    service =  LoginService();
    super.onInit();
  }



  void changeCheckBox() async {
    checkBoxStatus(!(checkBoxStatus.value));
  }

  Future<void> loginOnClick() async {
    User user = User(
      email: email,
      password: password,
    );
    loginStatus = await service.login(user, checkBoxStatus.value);
    message = service.message;

    if (message is List) {
      String temp = '';
      for (String s in message) temp += s + '\n';
      message = temp;
    }
  }
}
