import 'package:flutter_course/modules/splash/splash_controller.dart';
import 'package:get/get.dart';
import 'package:http/http.dart';

class SplashBinding implements Bindings{
  @override
  void dependencies() {
    Get.put<SplashController>((SplashController()));
    
  }
  
}