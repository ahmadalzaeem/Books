import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_course/constant.dart';

class CategoryCard extends StatelessWidget {
  final double height, width;
  final String text;
  final Function() onTap;

  CategoryCard({
    required this.text,
    required this.width,
    required this.height,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: height,
        width: width,
        color: Colors.white,
        child: Center(
          child: Text(
            '$text',
            style: TextStyle(
              fontSize: 25,
              fontFamily: 'Acaslon Regular',
            ),
          ),
        ),
      ),
    );
  }
}
